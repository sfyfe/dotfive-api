# Setup

Install [Node](https://nodejs.org/en/)

Install [MySQL](https://dev.mysql.com/downloads/mysql/) and create a new database

Add database connection and JSON web token secret into `config/local.js` datastores object. e.g.:
```
module.exports = {
  datastores: {
    default: {
      adapter: 'sails-mysql',
      host: 'localhost',
      database: 'dotfive-api',
      user: 'root',
      password: 'password'
    }
  },
  jwtSecret: 'N91RdlefZGktvhtXf17cex2MkOjkElzl'
}
```

Install project's dependencies and lift the sails!

```
cd path/to/project
npm i
sails lift
```

You should now see a welcome screen at [localhost:1337](http://localhost:1337)

# Testing

```
cd path/to/project
npm test
```
