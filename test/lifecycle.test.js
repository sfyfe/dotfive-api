let sails = require('sails')

before(function (done){
  this.timeout(5000)

  sails.lift({
    hooks: { grunt: false },
    log: { level: 'warn' },
  }, err => {
    return done(err)
  })
})

after(done => {
  sails.lower(done)
})
