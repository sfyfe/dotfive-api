const testEmail = 'thisisatest@email.com'

describe('User create', function () {

  describe('Create a user', function () {

    it('Should create a user', function (done) {

      User.create({email: testEmail, password: 'password'}).then(() => {
        User.findOne({email: testEmail}).exec((err, user) => {
          if (user.email !== testEmail) {
            done(new Error(`User wasn't created properly`))
          } else {
            // clean up after ourselves
            User.destroy({email: testEmail}).exec(err => {
              done(err)
            })
          }
        })
      })
      .catch(done)

    })

  })

})
