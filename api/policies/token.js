let jwt = require('jsonwebtoken')

module.exports = (req, res, next) => {
  let token = req.headers.authorization
  if (!token) {
    res.status(401)
    res.json({ message: 'No authentication token supplied' })
  } else {
    token = token.replace('Bearer ', '')

    jwt.verify(token, sails.config.jwtSecret, (err, decoded) => {
      if (err) {
        res.status(401)
        res.json({ message: 'No authentication token supplied' })
      } else {
        next()
      }
    })
  }
}
