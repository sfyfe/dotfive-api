let bcrypt = require('bcrypt')
let jwt = require('jsonwebtoken')

module.exports = {
  create: (req, res) => {
    // password is excluded from User.findOne, so do a raw query
    const sql = `SELECT * FROM user WHERE email = $1`
    const params = [req.param('email')]
    sails.getDatastore().sendNativeQuery(sql, params, (err, result) => {
      if (result.rows.length) {
        const hash = result.rows[0].password
        bcrypt.compare(req.param('password'), hash, (err, success) => {
          if (success) {
            User.findOne({email: req.param('email')})
            .populate('category').exec((err, user) => {
              const payload = {
                userID: user.id
              }
              const jwtOptions = {
                expiresIn: '2 days'
              }
              jwt.sign(payload, sails.config.jwtSecret, jwtOptions, (err, token) => {
                res.json({userID: user.id, token: token})
              })
            })
          } else {
            res.status(401)
            res.json({ message: 'Incorrect email or password' })
          }
        })
      } else {
        res.status(401)
        res.json({ message: 'Incorrect email or password' })
      }
    })
  }
}
