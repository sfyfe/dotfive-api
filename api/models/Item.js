module.exports = {
  attributes: {
    name: {
      type: 'string',
      required: true
    },
    category: {
      model: 'category',
      required: true
    },
    createdBy: {
      model: 'user',
      required: true
    }
  }
}
