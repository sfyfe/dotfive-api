let bcrypt = require('bcrypt')

module.exports = {
  attributes: {
    email: {
      unique: true,
      type: 'string',
      required: true,
      isEmail: true
    },
    password: {
      type: 'string',
      required: true
    },
    category: {
      collection: 'category',
      via: 'createdBy'
    }
  },
  beforeCreate: (userObj, next) => {
    // hash their password before storing
    bcrypt.hash(userObj.password, 10, (err, hash) => {
      userObj.password = hash
      next(err)
    })
  },
  customToJSON: function () {
    // ES6 arrow function won't get correct this

    // don't include password in result
    return {
      id: this.id,
      email: this.email,
      categories: this.category,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }
  }
}
