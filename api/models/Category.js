module.exports = {
  attributes: {
    name: {
      type: 'string',
      required: true,
      unique: true
    },
    item: {
      collection: 'item',
      via: 'category'
    },
    createdBy: {
      model: 'user',
      required: true
    }
  }
}
